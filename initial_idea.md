# Intitial Idea

## Ledger Wallet Nano Issues:

1. Have to write down a large passphrase from the tiny screen (hard to read)

2. Need to remember a 4 digit pin 

3. Private Key might actually be sent to a server from the client side of the app, so if the applications server is hacked while you are using it, others may be able to get the keys ( not very sure on this)




## Parts Required for 1st Prototype:

#### 1. Raspberry Pi 3b
##### Price: 197 rmb
https://world.taobao.com/item/535894042489.htm?spm=a312a.7700714.0.0.4UT9IP#detail


#### 2. Fingerprint Sensor 
##### Price: 299 rmb 
https://world.taobao.com/item/45506770989.htm?spm=a312a.7700714.0.0.wULPPx#detail



#### 3. Screen/Wires/Buttons
##### Price: 180 rmb 
https://world.tmall.com/item/42640098162.htm?spm=a312a.7700714.0.0.q0ptrl


## Basic Working Logic for 1st Prototype: 

1. Power on the device 

2. Connect to a screen and keyboard

3. Fill in the details of your transcation on the app that is booted on screen 

4. Click on Confirm

5. Now scan your fingerprint 

6. Verifaction Complete, Transaction Succesful 

## Actual Program Running on the Pi:

1. Custom Fingerprint Unlocker (need to program)   ETA: 1-2 days

2. Custome Remote Display (to display wallet info)  ETA: 1 week atleast 

3. Existing offline wallet such as Electrum or Armory ETA: 1-2 days 


## Issues with Current Design Logic for Prototype 1: 

1. We still are using the whole pi as a computer and need it to connect to the internet

2. We need to hook up a keyboard to the pi 

3. The raspberrypi will have to connect to internet in order to make any transactions (Only way to solve this would be to design a Client Application that pairs with the hardware but this will require more resources and will take a large amount of time )


## Best way to eliminate all these issues:

1. Will need more help and at least a few developers working full time in order to build a client app for a computer that links to the physical device since this has to be built completely on our own. 

2. Will need more hardware development boards and resources since eventhough the working logic of everything is simple, most of it has not been implemented before and there is no code base we can use in order to speed up our development time for both hardware and software. 

3. Estimated Resources Needed: Atleast 2-3 developers for the client program , 2-3 developers for the hardware 

4. Estimated Time if the above resources are met: Atleast 3 months (very optimistic guess) 


## Programmers Reference:

1. https://blog.adafruit.com/2014/05/09/create-your-own-secure-bitcoin-wallet-with-raspberry_pi-piday-raspberrypi/

2. https://bitcointalk.org/index.php?topic=1544915.0

3. https://bitcointalk.org/index.php?topic=1544726.0

4. https://www.seeed.cc/Door-to-Open-Source-Hardware-A-fingerprint-lock-solution--p-50.html

5. https://www.reddit.com/r/Bitcoin/comments/1rrxe5/howto_building_an_offline_cold_wallet_with_a/




