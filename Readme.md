# Bitcoin Cold Wallet 


## Investor Prototype: 

### How to use? 

![Illustration](usage_diagram.jpg)

### Parts Required 

#### 1. MCU (ESP32)
##### Price: 39 rmb
https://world.taobao.com/item/548905088891.htm?spm=a312a.7700714.0.0.x28zTh#detail

#### 2. Fingerprint Scanner 
##### Price: 299 rmb 
https://world.taobao.com/item/45506770989.htm?spm=a312a.7700714.0.0.wULPPx#detail

#### 3. Components (screen/wire/buttons)
##### Price: 180 rmb 
https://world.tmall.com/item/42640098162.htm?spm=a312a.7700714.0.0.q0ptrl

#### 4. More Parts 
##### More parts may be required, but I will only know after testing the fingerprint sensor with the MCU 


### Whats Actually Happening ? 

1. We are using this prototype only as an extra factor of authentication before any currency can be taken out of your offline wallet 

2. The offline wallet is just on the computer and the public and private keys are stored in the computer too (Not the Device)

3. The main reason for this is the time required of programming a software-server software-client, hardware-client solution to interact with the hardware is very large

4. This prototype is only a Visual one and not an Actual Cold Storage device since a simple python bitcoin wallet will be the offline storage device

### Estimated Time ? 

1. Hardware: The hardware should take about 2-2.5 weeks in order to finalize everything and be able to communicate with the fingerprint sensor 

2. Software: The software should take about 2-2.5 weeks in order to program everything 

3. Total Time: 5 weeks 




